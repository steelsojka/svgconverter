/**
 * Module replaces an SVG tag with a backup img tag if SVG is not supported
 */
;(function(exports, document) {

  var SVGConverter = {};
  /**
   * Method that performs the conversion
   * @param  {DOM} context Element to search within
   */
  SVGConverter.convert = function(context) {
    var i, len, backup, svg, img;
    var svgs = context ? context.getElementsByTagName('svg') : document.getElementsByTagName('svg');
    
    for (i = svgs.length - 1; i >= 0; i--) {
      svg     = svgs[i];
      backup  = svg.getAttribute('data-backup');
      img     = document.createElement('IMG');
      img.src = backup;

      svg.parentNode.insertBefore(img, svg);
      svg.parentNode.removeChild(svg);
    }
  };

  exports.SVGConverter = SVGConverter;

}(this, document));