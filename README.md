SVGConverter
============

Converts SVG tags to a backup image tag.  Useful if SVG is not supported by the browser.

How to
------
Add the attribute "data-backup" to your SVG tag with a path to your backup image.

```html
<svg version="1.1" data-backup="https://mysite.com/images/myLogo.png" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="191.856px" height="48.295px" viewBox="0 0 191.856 48.295" enable-background="new 0 0 191.856 48.295" xml:space="preserve">
```

After the page has loaded, just call the convert method on the SVGConverter object.

```javascript
$(function() {  // On ready
  SVGConverter.convert();
});

// or

window.addEventListener("DOMContenLoaded", function() {
  SVGConverter.convert();
});

// An optional context DOM element can be passed in to search within that element

window.addEventListener("DOMContenLoaded", function() {
  
  var header = document.getElementById("myHeader");

  SVGConverter.convert(header);
});
```